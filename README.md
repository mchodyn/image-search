This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Photo picker

## Jak uruchomić?

- Zainstaluj moduły `yarn` lub `npm install`
- Skopiuj i wypełnij plik ze zmiennymi środowiskowymi `cp .env.sample .env` (plik .env będzie już przygotowany - dla wygody)
- Wgraj zmeinne środowiskowe `source .env`
- Uruchom `npm run start` lub `yarn start`
- Aplikacja działa na adresie [http://localhost:3000](http://localhost:3000)

## Rozliczenie projetu

Dlaczego tak a nie inaczej, odniesienie się do wymagań projektu

### Elementy na stronie

- Nagłówek - jest obecny, jest dość prosty
- Pasek wyszukiwania - obecny w nagłowku
- Przycisk wyszukiwania - nie uważałem go za potrzebny, wyglądał mocno oldschoolowo
- Przycisk pozwalający wyszukać losowo - kompletnie nietrafiona funkcjonalność na podstawowym widoku wszystkie zdjęcia są losowe więc ten przycisk nie ma większego sensu.
- Grid view składający się z wyszukanych zdjęć - Obecny, nie jest idealny ale samodzielna implementacja była zbyt czasochłonna. Problemy głownie wychodziły z optymalizacji przez co przy większej ilości danych strasznie zacinało. Użyto gotowego komponentu
- Element „grida” zawierający miniaturę zdjęcia oraz podstawowe informacje jak np. autor. Element tez powinien posiadać funkcjonalność która pozwoli podejrzeć zdjęcie w większej rozdzielczości oraz przycisk który pozwoli na pobranie danego zdjęcia. (opcjonalnie) pobranie zdjęcia w wybranym formacie - Zrezygnowałem z tego aby pokazywać nazwe autora gdyż nie była ona zawsze dostępna plus nie wyglądało to dobrze. Pozostałe funkcjonalności dostępne
- Stopka - zdecydowałem sie na nieskończony scroll więc stopka stała się wręcz zbędna

### Wykorzystane technologie

- React + Typescript + (opcjonalnie jeśli zajdzie taka potrzeba) Redux - na szczęście redux nie był potrzebny, typescript był bardzo pomocny fajnie pilnował dobrych praktyk i jest fajnie czytelny

* SCSS’a lub Google Material Design (wybor zależy głownie od tego co Material Design może mi zaoferować) (opcjonalnie) Styled components jeśli okaze się ze stylowania koponentow jest tak mało ze nie będę widział sensu używania arkuszy styli i będzie to dobrze czytelne i łatwe w utrzymaniu - na początku fajnie wyglądał pomysł z styled components ale niestety przez brak wsparcia przez zewnętrzne biblioteki zrezygnowałem z użycia. Google materials nie spełniał moich wymagań więc wykorzystałem CSS'a gdyż SCSS nie dawał mi za wiele w tam małym projekcie

- Prettier- autokorekta kodu - obecny
- ES/TSLint - lintowanie kodu - obecny odpalany skryptem `npm run lint` lub `yarn lint`
- Webpack, Babel – budowanie projektu - dostarczona domyślna konfiguracja create-react-app'a
