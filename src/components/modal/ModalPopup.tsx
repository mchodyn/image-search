/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState } from 'react';
import Modal from 'react-modal';
import { Response } from '../../api/types';
import './ModalPopup.css';

interface ModalPopupProps {
  photo: Response;
}

enum ImageTypes {
  Thumb = 'thumb',
  Raw = 'raw',
  Full = 'full',
  Regular = 'regular',
  Small = 'small',
}

export const ModalPopup: React.FC<ModalPopupProps> = props => {
  Modal.setAppElement('#root');
  const [modalIsOpen, setIsOpen] = useState(false);

  const handleDownload = async (type: ImageTypes) => {
    let urlLink = '';

    switch (type as ImageTypes) {
      case ImageTypes.Thumb:
        urlLink = props.photo.urls.thumb;
        break;
      case ImageTypes.Raw:
        urlLink = props.photo.urls.raw;
        break;
      case ImageTypes.Full:
        urlLink = props.photo.urls.full;
        break;
      case ImageTypes.Regular:
        urlLink = props.photo.urls.regular;
        break;
      case ImageTypes.Small:
        urlLink = props.photo.urls.small;
        break;
    }

    const blob = await fetch(urlLink).then(r => r.blob());
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'image';
    document.body.appendChild(link);
    link.click();
    link.remove();
    return { success: true };
  };

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <div>
      <img
        className="img"
        onClick={openModal}
        src={props.photo.urls.thumb}
        alt=""
      />
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        overlayClassName="overlay"
        className="modal"
      >
        <img className="img-big" src={props.photo.urls.regular} alt="" />
        {/* TODO zrobic mapowanie po enumie */}
        <div className="button__wrapper">
          <button
            type="button"
            className="button__download"
            onClick={() => handleDownload(ImageTypes.Thumb)}
          >
            THUMB
          </button>
          <button
            type="button"
            className="button__download"
            onClick={() => handleDownload(ImageTypes.Small)}
          >
            SMALL
          </button>
          <button
            type="button"
            className="button__download"
            onClick={() => handleDownload(ImageTypes.Regular)}
          >
            REGULAR
          </button>
          <button
            type="button"
            className="button__download"
            onClick={() => handleDownload(ImageTypes.Full)}
          >
            FULL
          </button>
          <button
            type="button"
            className="button__download"
            onClick={() => handleDownload(ImageTypes.Raw)}
          >
            RAW
          </button>
        </div>
      </Modal>
    </div>
  );
};
