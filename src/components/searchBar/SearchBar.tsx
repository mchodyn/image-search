import React, { useState } from 'react';
import './SearchBar.css';

interface SearchBarProps {
  setQuery: React.Dispatch<React.SetStateAction<string>>;
}

export const SearchBar: React.FC<SearchBarProps> = props => {
  const [value, setValue] = useState<string>('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      props.setQuery(value);
    }
  };

  return (
    <input
      type="text"
      onKeyDown={handleKeyDown}
      onChange={handleChange}
      placeholder="Search for free stock photos"
    />
  );
};
