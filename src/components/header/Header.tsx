/* eslint-disable react/button-has-type */
import React from 'react';
import logo from '../../images/logo.png';
import { SearchBar } from '../searchBar/SearchBar';
import './Header.css';

interface HeaderProps {
  setQuery: React.Dispatch<React.SetStateAction<string>>;
}

export const Header: React.FC<HeaderProps> = props => {
  const scrollUp = () => {
    window.scrollTo(0, 0);
  };
  return (
    <header>
      <button onClick={scrollUp}>
        <img className="logo" src={logo} alt="" />
      </button>
      <SearchBar setQuery={props.setQuery} />
    </header>
  );
};
