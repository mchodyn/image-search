import React from 'react';
import { InfiniteScroll } from 'react-simple-infinite-scroll';
import Masonry from 'react-masonry-component';
import { ModalPopup } from '../modal/ModalPopup';
import { Response } from '../../api/types';

interface ScrollViewProps {
  isLoading: boolean;
  loadPhotos: () => Promise<void>;
  photos: Response[];
}

const options = { fitWidth: true, columnWidth: 200, gutter: 10 };

export const ScrollView: React.FC<ScrollViewProps> = props => {
  const { isLoading, loadPhotos, photos } = props;
  return (
    <InfiniteScroll
      throttle={2000}
      threshold={300}
      isLoading={isLoading}
      hasMore={true}
      onLoadMore={loadPhotos}
    >
      <Masonry
        elementType="div"
        options={options}
        disableImagesLoaded={false}
        updateOnEachImageLoad={false}
        className="container"
      >
        {photos.length > 0 &&
          photos.map(photo => <ModalPopup key={photo.id} photo={photo} />)}
      </Masonry>
    </InfiniteScroll>
  );
};
