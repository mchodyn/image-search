import React, { useState, useEffect } from 'react';
import { UnsplashApiClient } from './api/UnsplashApiClient';
import { Response } from './api/types';
import { ScrollView } from './components/scrollView/ScrollView';
import { Header } from './components/header/Header';
import { usePrevious } from './hooks/usePrevious';
import './App.css';

export const App: React.FC = () => {
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState(false);
  const [photos, setPhotos] = useState<Response[]>([]);
  const [query, setQuery] = useState<string>('');
  const api = new UnsplashApiClient();
  const prevQuery = usePrevious(query);

  const loadPhotos = async () => {
    if (!isLoading) {
      await getPhotos(prevQuery !== query);
    }
  };

  async function getPhotos(isNewSearch: boolean) {
    setIsLoading(true);

    if (isNewSearch) {
      query
        ? setPhotos(await api.getSpecifiedPhotos(page, query))
        : setPhotos(await api.getPhotos(1));
    } else {
      // make sure that there are no duplicates in photos api sometimes returns some
      const ids = new Set(photos.map(photo => photo.id));
      const response = query
        ? await api.getSpecifiedPhotos(page, query)
        : await api.getPhotos(page);
      setPhotos([...photos, ...response.filter(photo => !ids.has(photo.id))]);
    }

    setPage(page + 1);
    setIsLoading(false);
  }

  useEffect(() => {
    loadPhotos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (prevQuery !== query) {
      setPage(1);
      loadPhotos();
    }
  });

  return (
    <div className="App">
      <Header setQuery={setQuery} />
      <ScrollView
        isLoading={isLoading}
        loadPhotos={loadPhotos}
        photos={photos}
      />
      <h4>Loading</h4>
    </div>
  );
};
