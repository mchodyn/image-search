import axios, { AxiosInstance } from 'axios';
import { Response } from './types';

const API_URL = process.env.REACT_APP_UNSPLASH_API_URL;
const API_KEY = process.env.REACT_APP_UNSPLASH_API_KEY;
const PHOTOS_PER_PAGE = 30;

export class UnsplashApiClient {
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: API_URL,
      timeout: 5000,
      headers: {
        'Accept-Version': 'v1',
        Authorization: `Client-ID ${API_KEY}`,
      },
    });
  }

  async getPhotos(page: number): Promise<Response[]> {
    const response = await this.axiosInstance.get('/photos', {
      params: {
        per_page: PHOTOS_PER_PAGE,
        page,
      },
    });

    return response.data;
  }

  async getSpecifiedPhotos(page: number, query: string): Promise<Response[]> {
    const response = await this.axiosInstance.get('/search/photos', {
      params: {
        query,
        per_page: PHOTOS_PER_PAGE,
        page,
      },
    });

    return response.data.results;
  }
}
